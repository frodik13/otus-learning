﻿namespace MultiTreadingProject;

public static class CreatingArray
{
    public static IEnumerable<int> CreateArray(int count)
    {
        var random = new Random();
        for (int i = 0; i < count; i++)
        {
            yield return random.Next(100);
        }
    }
}