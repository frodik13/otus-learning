﻿namespace MultiTreadingProject;

public class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        var array = CreatingArray.CreateArray(100_000).ToList();

        Calculator.SequentialComputation(array);
        Calculator.ParallelThread(array);
        Calculator.ParallelToLinq(array);

        Console.WriteLine($"""

            {new string('=', 80)}

            """);

        array = CreatingArray.CreateArray(1_000_000).ToList();

        Calculator.SequentialComputation(array);
        Calculator.ParallelThread(array);
        Calculator.ParallelToLinq(array);

        Console.WriteLine($"""

            {new string('=', 80)}

            """);

        array = CreatingArray.CreateArray(10_000_000).ToList();

        Calculator.SequentialComputation(array);
        Calculator.ParallelThread(array);
        Calculator.ParallelToLinq(array);
    }
}
