﻿using System.Diagnostics;

namespace MultiTreadingProject;

public static class Calculator
{
    public static TimeSpan SequentialComputation(ICollection<int> array)
    {
        var stopwatch = Stopwatch.StartNew();

        var sum = 0;
        foreach (var item in array)
        {
            sum += item;
        }

        stopwatch.Stop();

        Console.WriteLine($"""
            {new string('-', 80)}
            Сумма последовательного вычисления = {sum}. Кол-во элементов = {array.Count}
            Время вычисления = {stopwatch.ElapsedMilliseconds} мс.
            {new string('-', 80)}
            """);

        return stopwatch.Elapsed;
    }

    public static TimeSpan ParallelToLinq(ICollection<int> array)
    {
        var stopwatch = Stopwatch.StartNew();

        var sum = array.AsParallel().Sum();

        stopwatch.Stop();

        Console.WriteLine($"""
            {new string('-', 80)}
            Сумма параллельно вычисления с помощью Linq = {sum}. Кол-во элементов = {array.Count}
            Время вычисления = {stopwatch.ElapsedMilliseconds} мс.
            {new string('-', 80)}
            """);

        return stopwatch.Elapsed;
    }

    public static TimeSpan ParallelThread(ICollection<int> array)
    {
        var numberOfThreads = 8;
        var threads = new List<Thread>();

        var partSize = array.Count / numberOfThreads;

        int sum = 0;
        object locker = new();
        
        var stopwatch = Stopwatch.StartNew();

        for (var i = 0; i < numberOfThreads; i++)
        {
            var start = i * partSize;
            var end = (i == numberOfThreads - 1) ? array.Count : start + partSize;

            var thread = new Thread(() =>
            {
                int partialSum = 0;
                for (int j = start; j < end; j++)
                {
                    partialSum += array.ElementAt(j);
                }

                lock (locker)
                {
                    sum += partialSum;
                }
            });
            thread.Start();
            threads.Add(thread);
        }

        threads.ForEach(thread => thread.Join());

        stopwatch.Stop();

        Console.WriteLine($"""
            {new string('-', 80)}
            Сумма параллельно вычисления с помощью Thread = {sum}. Кол-во элементов = {array.Count}
            Время вычисления = {stopwatch.ElapsedMilliseconds} мс.
            {new string('-', 80)}
            """);

        return stopwatch.Elapsed;
    }
}