﻿namespace MySerializer;

public class Foo
{
    public int Foo1 { get; set; } = 1;
    public int Foo2 { get; set; } = 2;
    public int Foo3 { get; set; } = 3;
    public int Foo4 { get; set; } = 4;
    public int Foo5 { get; set; } = 5;
    public List<int> Collection { get; set; } = new() { 1, 2, 3, 4, 5 };
    public NestedFoo FooNested { get; set; } = new();

    public List<NestedFoo> CollectionNested { get; set; } = new() { new NestedFoo(), new NestedFoo() };
}