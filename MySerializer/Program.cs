﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace MySerializer;

public class Program
{
    static void Main(string[] args)
    {
        const int repeatCount = 100000;
        var foo = new Foo();
        var stopwatch = Stopwatch.StartNew();
        for (int i = 0; i < repeatCount; i++)
        {
            _ = MySerializer.Serialize(foo);
        }
        Console.WriteLine($"Время на мою сериализацию: {stopwatch.ElapsedMilliseconds}ms," +
            $" Количество повторов: {repeatCount}");

        var str = MySerializer.Serialize(foo);

        stopwatch.Restart();
        for (int i = 0; i < repeatCount; i++)
        {
            _ = MySerializer.Deserialize<Foo>(str);
        }

        Console.WriteLine($"Время на мою десериализацию: {stopwatch.ElapsedMilliseconds}ms," +
            $" Количество повторов: {repeatCount}");

        Console.WriteLine();

        stopwatch.Restart();
        for (int i = 0; i < repeatCount; i++)
        {
            _ = JsonConvert.SerializeObject(foo);
        }

        Console.WriteLine($"Время на сериализацию Newtonsoft: {stopwatch.ElapsedMilliseconds}ms," +
            $" Количество повторов: {repeatCount}");

        str = JsonConvert.SerializeObject(foo);
        stopwatch.Restart();
        for (int i = 0; i < repeatCount; i++)
        {
            _ = JsonConvert.DeserializeObject<Foo>(str);
        }

        Console.WriteLine($"Время на десериализацию Newtonsoft: {stopwatch.ElapsedMilliseconds}ms," +
            $" Количество повторов: {repeatCount}");


    }
}
