﻿
using System.Collections;
using System.Text;

namespace MySerializer;

public class MySerializer
{
    public static T Deserialize<T>(string str) where T : class, new()
    {
        return (T)Deserialize(str, typeof(T));
    }

    public static object Deserialize(string str, Type type)
    {
        var result = Activator.CreateInstance(type);

        if (str.StartsWith('{') && str.EndsWith('}'))
        {
            str = str[1..^1];
            var keyValues = str.Split(';');
            foreach (var keyValue in keyValues)
            {
                var parts = keyValue.Split(':');
                var propertyName = parts[0][1..^1];
                var propertyValue = parts[1];
                if (parts.Length > 2)
                {
                    propertyValue = string.Join(':', parts[1..]);
                }

                var property = type.GetProperty(propertyName);
                if (property == null) continue;

                var propertyType = property.PropertyType;
                if (propertyType == typeof(string))
                {
                    property.SetValue(result, propertyValue);
                }
                else if (propertyType.IsPrimitive)
                {
                    var value = Convert.ChangeType(propertyValue.Trim(), propertyType);
                    property.SetValue(result, value);
                }
                else if (propertyType.GetInterface("IEnumerable") != null && propertyType.IsGenericType)
                {
                    var arrayValue = propertyValue[1..^1];
                    var values = arrayValue.Split(',');

                    Type listType = property.PropertyType.GetGenericArguments()[0];
                    IList collection = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(listType));

                    foreach (var value in values)
                    {
                        if (listType.IsClass)
                            collection.Add(Deserialize(value, listType));
                        else
                            collection.Add(Convert.ChangeType(value.Trim(), listType));
                    }

                    property.SetValue(result, collection);
                }
                else if (propertyType.IsClass)
                {
                    var nestedObj = Deserialize(propertyValue, propertyType);
                    property.SetValue(result, nestedObj);
                }
            }
        }
        return result;
    }

    public static string Serialize(object obj)
    {
        if (obj == null)
            return "null";

        var type = obj.GetType();
        var properties = type.GetProperties();
        var stringBuilder = new StringBuilder("{");
        foreach (var property in properties)
        {
            var value = property.GetValue(obj);
            stringBuilder.Append($"\"{property.Name}\":");
            if (value == null)
                stringBuilder.Append("null");
            else if (property.PropertyType == typeof(string))
                stringBuilder.Append($"\"{value}\"");
            else if (property.PropertyType.IsPrimitive)
                stringBuilder.Append($"{value}");
            else if (property.PropertyType.IsArray || (property.PropertyType.GetInterface("IEnumerable") != null))
            {
                stringBuilder.Append("[");

                var array = (IEnumerable)value;
                if (array == null)
                {
                    stringBuilder.Append("]");
                    continue;
                }
                foreach (var item in array)
                {
                    if (item.GetType().IsPrimitive)
                        stringBuilder.Append($"{item},");
                    else if (item.GetType() == typeof(string))
                        stringBuilder.Append($"\"{item}\",");
                    else
                        stringBuilder.Append(Serialize(item) + ",");
                }
                stringBuilder.Remove(stringBuilder.Length - 1, 1);
                stringBuilder.Append("]");
            }
            else
                stringBuilder.Append(Serialize(value));

            stringBuilder.Append(properties.Last() != property ? ";" : "}");
        }

        return stringBuilder.ToString();
    }
}