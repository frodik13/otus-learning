﻿namespace Prototype;

public interface IMyCloneable<T> where T : class
{
    T Clone();
}