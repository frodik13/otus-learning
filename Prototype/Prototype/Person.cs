﻿namespace Prototype;

/// <summary>
/// Базовый класс персоны.
/// </summary>
public class Person : IMyCloneable<Person>, ICloneable
{
    public string Name { get; }
    public int Age { get; }

    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public Person Clone()
    {
        return new Person(Name, Age);
    }

    object ICloneable.Clone()
    {
        return Clone();
    }
}