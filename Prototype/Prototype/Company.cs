﻿namespace Prototype;

/// <summary>
/// Класс компании.
/// </summary>
public class Company : IMyCloneable<Company>, ICloneable
{
    public Company(string city, string name)
    {
        City = city;
        Name = name;
    }

    public string City { get; }
    public string Name { get; set; }

    public Company Clone()
    {
        return new Company(City, Name);
    }

    object ICloneable.Clone()
    {
        return Clone();
    }
}
