﻿namespace Prototype;

/// <summary>
/// Класс студента.
/// </summary>
public class Student : Person, IMyCloneable<Student>
{
    public University University { get; set; }
    public int Cource { get; set; }

    public Student(string name, int age, University university, int cource) : base(name, age)
    {
        University = university;
        Cource = cource;
    }

    public new Student Clone()
    {
        return new Student(Name, Age, University.Clone(), Cource);
    }
}
