﻿namespace Prototype;

/// <summary>
/// Класс менеджера
/// </summary>
public class ProductManager : Employee, IMyCloneable<ProductManager>
{
    public IEnumerable<Employee> Subordinates { get; set; }

    public ProductManager(string name, int age, Company company, string position, IEnumerable<Employee> subordinates) : base(name, age, company, position)
    {
        Subordinates = subordinates;
    }

    public new ProductManager Clone()
    {
        var subordinates = Subordinates.Select(x => x.Clone());
        return new ProductManager(Name, Age, Company.Clone(), Position, subordinates);
    }
}
