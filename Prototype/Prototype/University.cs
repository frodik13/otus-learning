﻿namespace Prototype;

/// <summary>
/// Класс университета.
/// </summary>
public class University : IMyCloneable<University>, ICloneable
{
    public string City { get; }
    public string Name { get; }

    public University(string city, string name)
    {
        City = city;
        Name = name;
    }

    public University Clone()
    {
        return new University(City, Name);
    }

    object ICloneable.Clone()
    {
        return Clone();
    }
}
