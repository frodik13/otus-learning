﻿namespace Prototype;

/// <summary>
/// Класс работника
/// </summary>
public class Employee : Person, IMyCloneable<Employee>
{
    public Company Company { get; set; }
    public string Position { get; set; }

    public Employee(string name, int age, Company company, string position) : base(name, age)
    {
        Company = company;
        Position = position;
    }

    public new Employee Clone()
    {
        return new Employee(Name, Age, Company.Clone(), Position);
    }
}
