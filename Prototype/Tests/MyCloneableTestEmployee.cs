﻿using FluentAssertions;
using Prototype;

namespace Tests;

[TestFixture]
public class MyCloneableTestEmployee
{
    private Employee _original;

    [SetUp]
    public void SetUp()
    {
        // Arrange
        _original = new Employee("Fedor", 35, new Company("Izhevsk", "DySy"), "Programmer");
    }

    [Test]
    public void CloningClassFooAThereMustBeDifferentLink()
    {
        // Act
        var employeeClone = _original.Clone();

        // Assert
        _original.Should().NotBeSameAs(employeeClone);
    }

    [Test]
    public void CloningClassFooAPropertiesMustBeEquals()
    {
        // Act
        var employeeClone = _original.Clone();

        // Assert
        _original.Should().BeEquivalentTo(employeeClone);
    }

    [TearDown]
    public void TearDown()
    {
        _original = null;
    }
}