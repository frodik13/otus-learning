﻿using FluentAssertions;
using Prototype;

namespace Tests;

[TestFixture]
public class MyCloneableTestProductManager
{
    private ProductManager _original;

    [SetUp]
    public void SetUp()
    {
        // Arrange
        var company = new Company("Izhevsk", "DySy");
        var subordinates = new List<Employee> 
        {
            new Employee("Fedor", 35, company, "Programmer"),
            new Employee("Kirill", 35, company, "Programmer")
        };
        _original = new ProductManager("Grigory", 32, company, "Product Manager", subordinates);
    }

    [Test]
    public void CloningClassFooAThereMustBeDifferentLink()
    {
        // Act
        var productManagerClone = _original.Clone();

        // Assert
        _original.Should().NotBeSameAs(productManagerClone);
        _original.Company.Should().NotBeSameAs(productManagerClone.Company);
    }

    [Test]
    public void CloningClassFooAPropertiesMustBeEquals()
    {
        // Act
        var productManagerClone = _original.Clone();

        // Assert
        _original.Should().BeEquivalentTo(productManagerClone);
        productManagerClone.Subordinates.Count().Should().Be(2);
    }

    [TearDown]
    public void TearDown()
    {
        _original = null;
    }
}