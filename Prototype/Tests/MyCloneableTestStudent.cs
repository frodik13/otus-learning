﻿using FluentAssertions;
using Prototype;

namespace Tests;

[TestFixture]
public class MyCloneableTestStudent
{
    private Student _original;

    [SetUp]
    public void SetUp()
    {
        // Arrange
        _original = new Student("Fedor", 35, new University("Izhevsk", "Ipek"), 4);
    }

    [Test]
    public void CloningClassFooAThereMustBeDifferentLink()
    {
        // Act
        var studentClone = _original.Clone();

        // Assert
        _original.Should().NotBeSameAs(studentClone);
    }

    [Test]
    public void CloningClassFooAPropertiesMustBeEquals() 
    {
        // Act
        var studentClone = _original.Clone();

        // Assert
        _original.Should().BeEquivalentTo(studentClone);
    }

    [TearDown]
    public void TearDown()
    {
        _original = null;
    }
}