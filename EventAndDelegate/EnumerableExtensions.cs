﻿namespace EventAndDelegate;

public static class EnumerableExtensions
{
    public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> convertToNumber)
        where T : class
    {
        if (collection == null || !collection.Any())
        {
            throw new ArgumentNullException(nameof(collection));
        }

        T result = null;
        var maxValue = float.MinValue;
        foreach (var item in collection)
        {
            var value = convertToNumber(item);
            if (value > maxValue)
            {
                maxValue = value;
                result = item;
            }
        }

        return result;
    }
}