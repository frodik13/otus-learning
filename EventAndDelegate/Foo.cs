﻿namespace EventAndDelegate;

public class Foo
{
    public Foo(double doubleValue, int intValue)
    {
        DoubleValue = doubleValue;
        IntValue = intValue;
    }

    public double DoubleValue { get; }
    public int IntValue { get; }

    public override string ToString()
    {
        return $"Double = {DoubleValue}, Int = {IntValue}";
    }
}