﻿namespace EventAndDelegate;

public class InspectorFileCatalog
{
    public event EventHandler<FileArgs>? FileFound;

    public bool IsFileFound { get; set; }

    public void CrawlDirectory(string path)
    {
        IsFileFound = false;

        if (Directory.Exists(path))
        {
            var filesAndDirectories = Directory.GetFileSystemEntries(path);
            foreach (var file in filesAndDirectories)
            {
                if (!IsFileFound)
                {
                    if (Directory.Exists(file))
                        CrawlDirectory(file);
                    else if (File.Exists(file))
                        FileFound?.Invoke(this, new FileArgs(file));
                }
            }
        }
        else if (File.Exists(path))
            FileFound?.Invoke(this, new FileArgs(path));
    }
}