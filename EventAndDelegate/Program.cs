﻿namespace EventAndDelegate;

public class Program
{
    static void Main(string[] args)
    {
        var inspector = new InspectorFileCatalog();
        inspector.FileFound += Inspector_FileFound;
        inspector.CrawlDirectory(@"D:\Projects\repos\RestoRunner\Fedor");

        var random = new Random();
        List<Foo> list = new();
        for (var i = 0; i < 10; i++)
        {
            var doubleValue = random.NextDouble() * i;
            var intValue = random.Next(500);
            list.Add(new Foo(doubleValue, intValue));
        }

        var max = list.GetMax(x => x.IntValue * (float)x.DoubleValue);
        Console.WriteLine($"\n{new string('=', 80)}\n");
        Console.WriteLine(max);
    }

    private static void Inspector_FileFound(object? sender, FileArgs e)
    {
        if (!e.FilePath.EndsWith("EventAndDelegate.exe")) return;
        
        Console.WriteLine($"File found: {e.FilePath}\n");
        if (sender is InspectorFileCatalog inspector)
            inspector.IsFileFound = true;
    }
}