﻿using System.Diagnostics;

namespace ParallelReadFile;

public class Program
{
    static async Task Main(string[] args)
    {
        var stopwatch = Stopwatch.StartNew();

        var files = new List<string>() { "Test/TextFile1.txt", "Test/TextFile2.txt" , "Test/TextFile3.txt" };
        await Parallel.ForEachAsync(files, Read);

        Console.WriteLine($"Три файла прочитаны за {stopwatch.ElapsedMilliseconds} мс.");
        
        stopwatch.Restart();

        var count = await Reader.ReadDirectoryAsync("Test");

        Console.WriteLine($"Чтение директории Test заняло {stopwatch.ElapsedMilliseconds} мс. Количество пробелов = {count}");
    }

    private static async ValueTask Read(string file, CancellationToken token)
    {
        var spaceCount = await Reader.ReadFileAsync(file);
        Console.WriteLine($"В файле {file} {spaceCount} пробелов.");
    }
}
