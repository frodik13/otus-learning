﻿namespace ParallelReadFile;

public class Reader
{
    public static async Task<int> ReadFileAsync(string path)
    {
        var str = await File.ReadAllTextAsync(path);

        return str.Where(x => x == ' ').Count();
    }

    public static async Task<int> ReadDirectoryAsync(string directoryPath)
    {
        var files = Directory.GetFiles(directoryPath);
        var tasks = new Task<int>[files.Length];
        for (int i = 0; i < files.Length; i++)
        {
            var i1 = i;
            tasks[i1] = Task.Run(() => ReadFileAsync(files[i1]));
        }

        await Task.WhenAll(tasks);

        return tasks.Select(x => x.Result).Aggregate((a,b) => a + b);
    }
}