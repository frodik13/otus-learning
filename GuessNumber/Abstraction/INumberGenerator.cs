﻿namespace GuessNumber.Abstraction;

public interface INumberGenerator
{
    int GenerateNumber();
}