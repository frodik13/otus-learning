﻿namespace GuessNumber.Abstraction;

public interface IEditSettingsService
{
    void ReplaceRange();
    void ReplaceNumberOfAttemts();
    void EditSettings();
}