﻿namespace GuessNumber.Abstraction;

public interface IGame
{
    void StartGame();
}