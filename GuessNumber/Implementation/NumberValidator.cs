﻿using GuessNumber.Abstraction;

namespace GuessNumber.Implementation;

public class NumberValidator : INumberValidator
{
    public bool CheckNumber(int secretNumber, int enteredNumber, out string message)
    {
        if (enteredNumber < secretNumber)
        {
            message = "Введенное число меньше, чем загаданное.";
            return false;
        }
        else if (enteredNumber > secretNumber)
        {
            message = "Введенное число больше, чем загаданное.";
            return false;
        }
        else
        {
            message = "Поздравляю! Вы угадали число!";
            return true;
        }
    }
}