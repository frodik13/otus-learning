﻿using GuessNumber.Abstraction;

namespace GuessNumber.Implementation;

public class Game : IGame
{
    private readonly Settings _settings;
    private readonly INumberValidator _numberValidator;
    private readonly INumberGenerator _numberGenerator;

    public Game(Settings settings, INumberValidator numberValidator, INumberGenerator numberGenerator)
    {
        _settings = settings;
        _numberValidator = numberValidator;
        _numberGenerator = numberGenerator;
    }

    public void StartGame()
    {
        Console.WriteLine($"\nЗагадано число от {_settings.Range.Item1} до {_settings.Range.Item2}\n");

        var secretNumber = _numberGenerator.GenerateNumber();
        var numberOfAttemts = _settings.NumberOfAttemts;
        while (numberOfAttemts > 0)
        {
            Console.WriteLine($"Количество попыток: {numberOfAttemts}");
            Console.Write("Введите число или введите \"в\" если хотите закончить эту игру: ");
            string? enteredString = Console.ReadLine();

            if (string.Equals(enteredString, "в", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine($"Было загадано число {secretNumber}");
                return;
            }

            if (!int.TryParse(enteredString, out var enteredNumber))
                Console.WriteLine("Введенная строка не распознана. Попробуйте еще.");
            else
            {
                var result = _numberValidator.CheckNumber(secretNumber, enteredNumber, out var message);
                Console.WriteLine(message + "\n");

                if (result)
                    return;
                else
                    numberOfAttemts--;
            }

            Console.WriteLine();
        }

        Console.WriteLine($"Вы не угадали число. Загаданное число было {secretNumber}");
    }
}