﻿using GuessNumber.Abstraction;

namespace GuessNumber.Implementation;

public class ConsoleEditSettingsService : IEditSettingsService
{
    private readonly Settings _settings;
    private readonly ISettingsRepository _repository;

    public ConsoleEditSettingsService(Settings settings, ISettingsRepository repository)
    {
        _settings = settings;
        _repository = repository;
    }

    public void EditSettings()
    {
        while (true)
        {
            Console.Write($"Введите \"п\" для изменения количества попыток, \n" +
                $"\"р\" - для изменения диапазона и \"с\" для того чтобы сохранить изменения и выйти: ");

            var enteredString = Console.ReadLine();

            if (string.Equals(enteredString, "п", StringComparison.OrdinalIgnoreCase))
                ReplaceNumberOfAttemts();
            else if (string.Equals(enteredString, "р", StringComparison.OrdinalIgnoreCase))
                ReplaceRange();
            else if (string.Equals(enteredString, "с", StringComparison.OrdinalIgnoreCase))
            {
                if (!_repository.SaveSettings(_settings, "Settings.json", out var errorMessage))
                    Console.WriteLine(errorMessage);
                return;
            }
        }
    }

    public void ReplaceNumberOfAttemts()
    {
        while (true)
        {
            Console.Write("Введите новое количество попыток: ");
            var enteredString = Console.ReadLine();
            if (int.TryParse(enteredString, out var numberOfAttemts))
            {
                _settings.NumberOfAttemts = numberOfAttemts;
                return;
            }
            else
            {
                Console.WriteLine("Не удалось распознать число. Попробуйте снова.");
            }
        }
    }

    public void ReplaceRange()
    {
        while (true)
        {
            Console.Write("Введите новый диапазон значений загадываемого числа." +
                "\n\tВведите два числа через пробел: ");
            var enteredString = Console.ReadLine();

            if (enteredString == null)
            {
                Console.WriteLine("Ошибка ввода. Попробуйтe снова");
                continue;
            }

            var rangeString = enteredString.Split(" ");

            if (rangeString.Length != 2)
            {
                Console.WriteLine("Введено не 2 числа. Попробуйтe снова");
                continue;
            }

            if (!TryParse(rangeString, out var range))
            {
                Console.WriteLine("Ошибка при конвертации числа. Попробуйтe снова");
                continue;
            }

            _settings.Range = new(range[0], range[1]);

            return;
        }
    }

    private static bool TryParse(string[] rangeString, out int[] range)
    {
        range = new int[2];
        for (int i = 0; i < rangeString.Length; i++)
        {
            if (!int.TryParse(rangeString[i], out range[i]))
            {
                return false;
            }
        }

        range = range.OrderBy(x => x).ToArray();

        return true;
    }
}