﻿namespace GuessNumber;

public class Settings
{
    public int NumberOfAttemts { get; set; } = 5;

    public Tuple<int, int> Range { get; set; } = new(0, 100);
}
