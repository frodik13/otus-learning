﻿namespace GuessNumber.Implementation;

public class Program
{
    static void Main(string[] args)
    {
        var settingsRepository = new JsonSettingsRepository();
        var settings = settingsRepository.ReadSettings("Settings.json");
        var editSettingsService = new ConsoleEditSettingsService(settings, settingsRepository);
        var readLine = string.Empty;
        while (true)
        {
            Console.Write("Введите \"с\" чтобы начать игру, \"н\" чтобы войти в настройки или \"в\" для выхода: ");
            readLine = Console.ReadLine();
            if (string.Equals(readLine, "с", StringComparison.OrdinalIgnoreCase))
            {
                var generator = new RandomNumberGeneratorOfRange(settings);
                var validator = new NumberValidator();
                var game = new Game(settings, validator, generator);
                game.StartGame();
            }
            else if (string.Equals(readLine, "н", StringComparison.OrdinalIgnoreCase))
            {
                editSettingsService.EditSettings();
            }
            else if (string.Equals(readLine, "в", StringComparison.OrdinalIgnoreCase))
                return;

            Console.WriteLine("\n");
        }
    }
}
